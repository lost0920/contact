function addLoadEvent(func) {
    var oldonload=window.onload;
    if(typeof window.onload != 'function')
    {
        window.onload=func;
    }
    else
    {
        window.onload=function()
        {
            oldonload();
            func();
        }
    }
}
addLoadEvent(rr_router);
function rr_router(){
    var rr_about=Vue.extend({
        template:' <div class="rr-content-right-p">'+
        '<p>PAYMAx为您提供一站式支付方式，通过SDK，所有交易PAYMAX为您提供一站式支付接入解决方案，'+
        '您可任意连接PAYMAX的支付通道，并通过交易管理平台监控。</p>'+
        '<img src="images/rr-head-img3.png" alt="">'+
        '<div class="rr-content-right-app">'+
        '<h3>移动APP支付:</h3>'+
        '<span>您已经有自己的APP，您的用户选择支付按钮后，在对应的APP完成支付，在跳回您的应用。</span>'+
        '<ul>'+
        '<li>-APPLE PAY:只试用phone手机，在手机中完成支付。</li>'+
        '<li>-支付宝:手机中有支付宝客户端，在手机中完成支付。</li>'+
        '<li>-微信支付:手机中有微信客户端，在手机中完成支付。</li>'+
        '<li>-SDK支付:用户支付无需跳转，在手机中完成支付。</li>'+
        '</ul>'+
        '</div>'+
        '<div class="rr-content-right-app">'+
        '<h3>移动端支付:</h3>'+
        '<span>您已经有自己的APP，您的用户选择支付按钮后，在对应的APP完成支付，在跳回您的应用。</span>'+
        '<ul>'+
        '<li>-APPLE PAY:只试用phone手机，在手机中完成支付。</li>'+
        '<li>-支付宝:手机中有支付宝客户端，在手机中完成支付。</li>'+
        '<li>-微信支付:手机中有微信客户端，在手机中完成支付。</li>'+
        '<li>-SDK支付:用户支付无需跳转，在手机中完成支付。</li>'+
        '</ul>'+
        '</div>'+
        '</div>'
    });
    var rr_contact=Vue.extend({
        template:'<div class="rr_contact-c"><ul>' +
        '<li>tel:010-88888888</li>' +
        '<li>email:123456@qq.com</li>' +
        '</ul></div>'
    });
    var rr_join=Vue.extend({
        template:'<h3>加入</h3>'
    });
    var rr_news=Vue.extend({
        template:'<h3>新闻</h3>'
    });
    var App=Vue.extend({});
    var Router=new VueRouter();
    Router.map({
        '/rr_about':{
            component:rr_about
        },
        '/rr_contact':{
            component:rr_contact
        },
        '/rr_join':{
            component:rr_join
        },
        '/rr_news':{
            component:rr_news
        }
    });
    Router.start(App,'#rr_content_router');
}
